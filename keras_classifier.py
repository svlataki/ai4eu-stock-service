import pandas as pd
import numpy as np 
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn import metrics
import joblib


import tensorflow as tf
from tensorflow import keras
from tensorflow.python.keras import layers

def main(data_dir):
    df = pd.read_csv(data_dir)
    # We drop features that are either redundant(suplier) or have no ML value (timestamps)
    df = df.drop(['Supplier', 'Day','Transport timestamp'], 1)

    print(df.columns)
    X = df.iloc[:,:-1]
    y = df.iloc[:,-1]
     
    # Label encode YES/NO output
    le = preprocessing.LabelEncoder()
    le.fit(y)
    y = le.transform(y)

    # split for training and validation
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    # create instance and train
    model = keras.Sequential()
    
    model.add(layers.Flatten(input_shape=(25,)))
    model.add(layers.Dense(50, activation='relu' ))
    model.add(layers.Dense(1, activation='sigmoid'))

    # Compile the model
    model.compile(optimizer='adam', 
                loss='binary_crossentropy', 
                metrics=['accuracy'])

    model.fit(X_train.values.astype(float), y_train, epochs=40)

    # make predictions
    y_pred = model.predict_classes(X_test.values.astype(float))
   
    # Model Accuracy, how often is the classifier correct?
    print("Accuracy:",metrics.accuracy_score(y_test, y_pred.flatten()))
    model.save('keras_model.h5')

if __name__ == "__main__":
    main(r'anonymized_data.csv')