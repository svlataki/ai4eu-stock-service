FROM python:3.6

RUN mkdir /ai4eu

WORKDIR /ai4eu

COPY requirements.txt /ai4eu/

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8061

COPY model.proto model_pb2.py model_pb2_grpc.py /ai4eu/

COPY critical_component_prediction_server.py /ai4eu/

COPY keras_classifier.py keras_model.h5 model.py  /ai4eu/

RUN useradd app

USER app

CMD ["python", "critical_component_prediction_server.py"]

