from sklearn.ensemble import RandomForestClassifier
import joblib
from tensorflow import keras
from tensorflow.python.keras.models import load_model
import numpy as np

def predict_critical_component(Part, Manager, Suppliernr, Distance, Inspection_Advance, Safety_Stock_Advance, Minimum_Coverage, Demand, Calloff,       
       Stock, Material_on_transport, Material_in_house, transport_age, Stock_end_of_day, Shipment_concept, Forecast_day_1,Forecast_day_2, Forecast_day_3, 
       Forecast_day_4, Forecast_day_5, Backlog, SUPPLIERPOSTALCODE, SUPPLIERCITY, SUPPLIERREGION,SUPPLIERCOUNTRY):
   #trained_model = joblib.load('classifier.pkl')
   trained_model = load_model('keras_model.h5')

   X = np.array([[Part, Manager, Suppliernr, Distance, Inspection_Advance,     
      Safety_Stock_Advance, Minimum_Coverage, Demand, Calloff,       
      Stock, Material_on_transport, Material_in_house, transport_age,
      Stock_end_of_day, Shipment_concept, Forecast_day_1,
      Forecast_day_2, Forecast_day_3, Forecast_day_4, Forecast_day_5,
      Backlog, SUPPLIERPOSTALCODE, SUPPLIERCITY, SUPPLIERREGION,     
      SUPPLIERCOUNTRY]])
   X = X.astype(float)
   critical = trained_model.predict_classes(X)
   keras.backend.clear_session() 
   return (critical)
