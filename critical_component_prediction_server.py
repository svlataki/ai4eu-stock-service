# titanic_survived_prediction_server.py
import grpc
from concurrent import futures
import time# import grpc class
import model_pb2_grpc
import model_pb2#import predict function
import model

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class PredictServicer(model_pb2_grpc.PredictServicer):
    def predict_critical_component(self, request, context):
        response =  model_pb2.Prediction()
        response.critical = model.predict_critical_component(request.part, request.manager, request.suppliernr, request.distance, request.inspection_advance, request.safety_stock_advance,request.minimum_coverage,
                request.demand, request.calloff, request.stock, request.material_on_transport, request.material_in_house,request.transport_age, request.stock_end_of_day, 
                request.shipment_concept, request.forecast_day_1,request.forecast_day_2, request.forecast_day_3, request.forecast_day_4, request.forecast_day_5,
                request.backlog, request.supplier_postal_code, request.supplier_city, request.supplier_region, request.supplier_country)
        return response
        
def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=4))

    model_pb2_grpc.add_PredictServicer_to_server(PredictServicer(), server)
    print('Starting server. Listening on port 8061.')
    server.add_insecure_port('localhost:8061')

    server.start()

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()